package com.mushroom.midnight.common.mixin;

import com.mushroom.midnight.common.registry.MidnightBlocks;
import com.mushroom.midnight.common.registry.MidnightItems;
import net.minecraft.client.util.RecipeBookCategories;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.gen.Invoker;

import java.util.ArrayList;
import java.util.Arrays;

@Mixin(RecipeBookCategories.class)
@Unique
public abstract class RecipeBookCategoriesMixin {
    @Shadow(remap = false)
    private @Final @Mutable static RecipeBookCategories[] $VALUES;

    static {
		add("MIDNIGHT_FURNACE_SEARCH", new ItemStack(Items.COMPASS));
		add("MIDNIGHT_FURNACE_FOOD", new ItemStack(MidnightItems.RAW_STAG_FLANK));
		add("MIDNIGHT_FURNACE_BLOCKS", new ItemStack(MidnightBlocks.NIGHTSTONE));
		add("MIDNIGHT_FURNACE_MISC", new ItemStack(MidnightItems.MIASMA_BUCKET), new ItemStack(MidnightItems.TENEBRUM_INGOT));
    }

    @Invoker("<init>")
    public static RecipeBookCategories create(String internalName, int internalId, ItemStack... items) {
        throw new AssertionError();
    }

    private static RecipeBookCategories add(String internalName, ItemStack... items) {
        ArrayList<RecipeBookCategories> variants = new ArrayList<>(Arrays.asList($VALUES));
        RecipeBookCategories value = create(internalName, variants.get(variants.size() - 1).ordinal() + 1, items);
        variants.add(value);
        $VALUES = variants.toArray(new RecipeBookCategories[0]);
        return value;
    }
}
