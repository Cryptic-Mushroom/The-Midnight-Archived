package com.mushroom.midnight.common.mixin;

import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.server.ChunkHolder;
import net.minecraft.world.server.ChunkManager;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

@Mixin(ChunkManager.class)
public interface ChunkManagerAccessor {
    @Invoker
    Iterable<ChunkHolder> invokeGetLoadedChunksIterable();

    @Invoker
    boolean invokeIsOutsideSpawningRadius(ChunkPos chunkPosIn);
}
