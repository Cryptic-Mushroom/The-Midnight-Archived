package com.mushroom.midnight.common.mixin;

import com.mushroom.midnight.common.item.tool.MidnightAxeItem;
import net.minecraft.block.Block;
import net.minecraft.item.AxeItem;
import net.minecraft.item.ItemUseContext;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyVariable;

@Mixin(AxeItem.class)
public abstract class AxeItemMixin {
    @ModifyVariable(
            method = "onItemUse(Lnet/minecraft/item/ItemUseContext;)Lnet/minecraft/util/ActionResultType;",
            at = @At("STORE"),
            index = 5
    )
    private Block checkForMidnightStrippables(Block block, ItemUseContext context) {
        // this is really shitty and effectively gets the blockstate AGAIN from earlier
        // but i don't have it in me to make an efficient solution for an unsupported version of the game
        return block == null
                ? MidnightAxeItem.MIDNIGHT_BLOCK_STRIPPING_MAP.get(context.getWorld().getBlockState(context.getPos()).getBlock())
                : block;
    }
}
