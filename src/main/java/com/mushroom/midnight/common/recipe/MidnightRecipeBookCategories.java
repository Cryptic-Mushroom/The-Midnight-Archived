package com.mushroom.midnight.common.recipe;

import net.minecraft.client.util.RecipeBookCategories;

/**
 * This class holds the references to our custom recipe book category. They are directly added in
 * {@link com.mushroom.midnight.common.mixin.RecipeBookCategoriesMixin RecipeBookCategoriesMixin}.
 *
 * @author Jonathing
 * @see com.mushroom.midnight.common.mixin.RecipeBookCategoriesMixin
 */
public class MidnightRecipeBookCategories {
    public static final RecipeBookCategories FURNACE_SEARCH = RecipeBookCategories.valueOf("MIDNIGHT_FURNACE_SEARCH");
    public static final RecipeBookCategories FURNACE_FOOD = RecipeBookCategories.valueOf("MIDNIGHT_FURNACE_FOOD");
    public static final RecipeBookCategories FURNACE_BLOCKS = RecipeBookCategories.valueOf("MIDNIGHT_FURNACE_BLOCKS");
    public static final RecipeBookCategories FURNACE_MISC = RecipeBookCategories.valueOf("MIDNIGHT_FURNACE_MISC");
}
