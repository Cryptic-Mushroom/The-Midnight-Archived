package com.mushroom.midnight.common.loot;

import com.mushroom.midnight.Midnight;
import com.mushroom.midnight.common.mixin.LootPoolAccessor;
import com.mushroom.midnight.common.mixin.LootTableAccessor;
import com.mushroom.midnight.common.util.MidnightUtil;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.storage.loot.LootPool;
import net.minecraft.world.storage.loot.LootTable;
import net.minecraft.world.storage.loot.LootTables;
import net.minecraft.world.storage.loot.conditions.ILootCondition;
import net.minecraft.world.storage.loot.conditions.LootConditionManager;
import net.minecraftforge.event.LootTableLoadEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.LogicalSide;
import net.minecraftforge.fml.LogicalSidedProvider;
import net.minecraftforge.fml.common.Mod;

import java.util.List;

import static com.mushroom.midnight.Midnight.MODID;

@Mod.EventBusSubscriber(modid = MODID, bus = Mod.EventBusSubscriber.Bus.FORGE)
public class FishingLoot {
    private static final ResourceLocation MIDNIGHT_FISHING = new ResourceLocation(MODID, "fishing");

    @SubscribeEvent(priority = EventPriority.LOWEST)
    public static void onLootTableLoad(LootTableLoadEvent event) {
        if (event.getName().equals(LootTables.GAMEPLAY_FISHING)) {
            MinecraftServer server = LogicalSidedProvider.INSTANCE.get(LogicalSide.SERVER);
            server.deferTask(() -> {
                LootTable midnightFishing = event.getLootTableManager().getLootTableFromLocation(MIDNIGHT_FISHING);
                if (midnightFishing == LootTable.EMPTY_LOOT_TABLE) {
                    Midnight.LOGGER.warn("The loottable for midnight fishing is absent");
                } else {
                    try {
                        // because modded loot tables are not fired here
                        ((LootTableAccessor) midnightFishing).setFrozen(false);
                        ((LootTableAccessor) event.getTable()).setFrozen(false);
                        addConditionToAllMainPools(event.getTable(), context -> !MidnightUtil.isMidnightDimension(context.getWorld()));

                        LootPool midnightPool = midnightFishing.getPool("midnight_fishing");
                        addConditionToPool(context -> MidnightUtil.isMidnightDimension(context.getWorld()), midnightPool);
                        event.getTable().addPool(midnightPool);

                        ((LootTableAccessor) event.getTable()).setFrozen(true);
                        ((LootTableAccessor) midnightFishing).setFrozen(true);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private static void addConditionToAllMainPools(LootTable table, ILootCondition condition) throws IllegalAccessException {
        List<LootPool> pools = ((LootTableAccessor) table).getPools();
        if (pools != null) {
            for (LootPool pool : pools) {
                addConditionToPool(condition, pool);
            }
        }
    }

    private static void addConditionToPool(ILootCondition condition, LootPool pool) throws IllegalAccessException {
        List<ILootCondition> conditions = ((LootPoolAccessor) pool).getConditions();
        conditions.add(condition);
        ((LootPoolAccessor) pool).setCombinedConditions(LootConditionManager.and(conditions.toArray(new ILootCondition[0])));
    }
}
