package com.mushroom.midnight.common.item.tool;

import com.google.common.collect.ImmutableMap;
import com.mushroom.midnight.common.registry.MidnightBlocks;
import net.minecraft.block.Block;
import net.minecraft.item.AxeItem;
import net.minecraft.item.IItemTier;
import java.util.Map;

public class MidnightAxeItem extends AxeItem {
    public static final Map<Block, Block> MIDNIGHT_BLOCK_STRIPPING_MAP =
            new ImmutableMap.Builder<Block, Block>()
                    .put(MidnightBlocks.SHADOWROOT_LOG, MidnightBlocks.SHADOWROOT_STRIPPED_LOG)
                    .put(MidnightBlocks.DARK_WILLOW_LOG, MidnightBlocks.DARK_WILLOW_STRIPPED_LOG)
                    .put(MidnightBlocks.DEAD_WOOD_LOG, MidnightBlocks.DEAD_WOOD_STRIPPED_LOG)
                    .put(MidnightBlocks.SHADOWROOT_WOOD, MidnightBlocks.SHADOWROOT_STRIPPED_WOOD)
                    .put(MidnightBlocks.DARK_WILLOW_WOOD, MidnightBlocks.DARK_WILLOW_STRIPPED_WOOD)
                    .put(MidnightBlocks.DEAD_WOOD, MidnightBlocks.DEAD_WOOD_STRIPPED)
                    .build();

    public MidnightAxeItem(IItemTier tier, Properties properties) {
        super(tier, 6f, -3.2f, properties);
    }
}
